<?php
/**
 * @file
 * Ctools content type for rendering a form by id.
 *
 * This will not work for all forms.
 * You should probably know how the Drupal Form system works.
 */

  /**
   * Plugins are described by creating a $plugin array which will be used
   * by the system that includes this file.
   */
  $plugin = array(
    'single' => TRUE,
    'title' => t('Form by Id'),
    'icon' => 'icon_node.png',
    'description' => t('Embed a form by it\'s id.'),
    'render callback' => 'panels_forms_by_id_type_render',
    'admin title' => 'panels_forms_by_id_type_admin_title',
    'category' => t('Widgets'),
    'defaults' => array(
      'render_form_id' => '',
    ),
    'edit form' => 'panels_forms_by_id_type_edit_form',
  );

function panels_forms_by_id_type_render($subtype, $conf, $panel_args, $context) {
  $block = new stdClass();
  $block->module = 'panels_forms_by_id';
  $block->delta  = $conf['render_form_id'];

  $block->title = '';
  $block->content = drupal_get_form($conf['render_form_id']);
  return $block;
}

function panels_forms_by_id_type_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];
  $form['render_form_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Form Id'),
    '#default_value' => $conf['render_form_id'],
    '#weight' => 1,
    '#description' => t('Provide the form_id for the form to be rendered. This will not work with every form and there is no check that from exists. Be careful!'),
  );
  // @todo add redirect option
  return $form;
}

function panels_forms_by_id_type_edit_form_submit($form, &$form_state) {
  // Copy everything from our defaults.
  foreach (array_keys($form_state['plugin']['defaults']) as $key) {
    $form_state['conf'][$key] = $form_state['values'][$key];
  }
}

function panels_forms_by_id_type_admin_title($subtype, $conf, $context) {
  return t('Rendered Form: @form', array('@form' => $conf['render_form_id']));
}

